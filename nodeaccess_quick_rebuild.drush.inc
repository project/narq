<?php

function nodeaccess_quick_rebuild_drush_command(){
  $items = array();

  $items['nodeaccess-quick-rebuild'] = array(
    'callback' => '_nodeaccess_quick_rebuild',
    'description' => "Performs a quick rebuild of the node access permissions",
    'arguments' => array(
    ),
    'drupal dependencies' => array(),
    'aliases' => array('narq'),
  );

  return $items;
  
}

function nodeaccess_quick_rebuild_drush_help($section) {
  switch ($section) {
    case 'drush:nodeaccess-quick-rebuild':
      return dt("Quickly rebuild the node access permissions on your site.");
  }
}
