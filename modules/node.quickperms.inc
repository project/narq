<?php

function node_quick_rebuild(){
  db_query("
  insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
  select n.nid, 0, 'all', 1, 0, 0
  from {node} n left join {node_access} na on n.nid=na.nid
  where na.nid is null
  ");
}
