<?php
function og_access_quick_rebuild(){
  foreach(node_get_types() as $type=>$data){
    if (og_is_group_type($type)) {
      // This grant allows group admins to manage their group.
      db_query("
      insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
      select n.nid, n.nid, 'og_admin', 1, 1, 0
      from {node} n 
      where n.type='%s'
      ",$type);
      
      // If the group is not private, let everyone view the group homepage.
      db_query("
      insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
      select n.nid, 0, 'og_public', 1, 0, 0
      from {node} n
        left join {og} og on n.nid=og.nid 
      where n.type='%s' and coalesce(og.og_private,0)=0
      ",$type);

      // If the group is private, let subscribers view the group homepage.
      db_query("
      insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
      select n.nid, n.nid, 'og_subscriber', 1, 0, 0
      from {node} n
        inner join {og} og on n.nid=og.nid 
      where n.type='%s' and og.og_private!=0
      ",$type);
      
    }
    else{
      $is_wiki=og_is_wiki_type($type);
      db_query("
      insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
      select n.nid, oa.group_nid, 'og_subscriber', 1, %d, 0
      from {node} n
        left join {og} og on n.nid=og.nid 
        inner join {og_ancestry} oa on n.nid=oa.nid
      where n.type='%s'
      ",$is_wiki, $type);
    }
  }
  
  // posts in at least one group and marked as public get the og_public entry
  db_query("
  insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
  select distinct n.nid, 0, 'og_public', 1, 0, 0
  from {node} n
    left join {og} og on n.nid=og.nid 
    inner join {og_ancestry} oa on n.nid=oa.nid
    left join {og_access_post} oap on n.nid=oap.nid
  where coalesce(oap.og_public, 1)=1
  ");

  // Group administrators get all operations.
  db_query("
  insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
  select n.nid, oa.group_nid, 'og_admin', 1, 1, 1
  from {node} n
    left join {og} og on n.nid=og.nid 
    inner join {og_ancestry} oa on n.nid=oa.nid
  ");
}