<?php

function workflow_access_quick_rebuild(){
  // build the 'author' permissions
 db_query("
 insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
 select wn.nid, 1, 'workflow_access_owner', wa.grant_view, wa.grant_update, wa.grant_delete
 from {workflow_node} wn
   inner join {workflow_access} wa on wn.sid=wa.sid
 where 
   wa.rid=-1 and (wa.grant_view <> 0 or wa.grant_update <> 0 or wa.grant_delete <> 0)
 "); 
 // build the role based permissions 
 db_query("
 insert into {node_access}(nid, gid, realm, grant_view, grant_update, grant_delete)
 select wn.nid, wa.rid, 'workflow_access', wa.grant_view, wa.grant_update, wa.grant_delete
 from {workflow_node} wn
   inner join {workflow_access} wa on wn.sid=wa.sid
 where wa.rid > 0 and (wa.grant_view <> 0 or wa.grant_update <> 0 or wa.grant_delete <> 0)
 "); 
}